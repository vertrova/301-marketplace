package com.itau.marketplace.services;

import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;

@Service
public class JWTService {
	
	String key = "marketplace";
	Algorithm  algorithm = Algorithm.HMAC256(key);
	JWTVerifier verifier =  JWT.require(algorithm).build();
	
	public String gerarToken(String username) {
		return JWT.create().withClaim("username",username).sign(algorithm);
	}
	
	public String verificarToken(String token) {
		return verifier.verify(token).getClaim("username").asString();
	}

}
