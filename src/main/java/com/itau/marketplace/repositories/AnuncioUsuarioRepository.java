package com.itau.marketplace.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.marketplace.models.AnuncioUsuario;

public interface AnuncioUsuarioRepository extends CrudRepository<AnuncioUsuario,String>{

	public Iterable<AnuncioUsuario> findByUsername(String username);

}
