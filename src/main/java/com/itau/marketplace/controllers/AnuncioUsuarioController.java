package com.itau.marketplace.controllers;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.marketplace.models.AnuncioUsuario;
import com.itau.marketplace.models.Messagem;
import com.itau.marketplace.models.Usuario;
import com.itau.marketplace.repositories.AnuncioUsuarioRepository;
import com.itau.marketplace.repositories.UsuarioRepository;
import com.itau.marketplace.services.JWTService;

@RestController
public class AnuncioUsuarioController {
	
	@Autowired
	AnuncioUsuarioRepository anuncioUsuarioRepository;
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	JWTService jwtService;
	
		
	@RequestMapping(path="/anuncios/usuario")
	public ResponseEntity<?> buscarAnunciosUsuario(HttpServletRequest request){
		String token = request.getHeader("Authorization");
		token = token.replace("Bearer ", "");
		
		String username = jwtService.verificarToken(token);
		
		if(username == null) {
			return ResponseEntity.status(403).body(new Messagem("Token Invalido"));
		}
		
		Optional<Usuario> usuarioBanco = usuarioRepository.findByUsername(username);
		
		if(!usuarioBanco.isPresent()) {
			return ResponseEntity.status(403).body(new Messagem("Usuario nao encontrado."));
		}
		
		Iterable<AnuncioUsuario> anuncionUsuarioBanco = anuncioUsuarioRepository.findByUsername(usuarioBanco.get().getUsername());
		
		if(anuncionUsuarioBanco == null) {
			return null;
		}
		
		return ResponseEntity.ok().body(anuncionUsuarioBanco);
		
	}
	 
	

}
