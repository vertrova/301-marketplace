package com.itau.marketplace.controllers;

import java.util.Optional;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.marketplace.models.Usuario;
import com.itau.marketplace.repositories.UsuarioRepository;
import com.itau.marketplace.services.JWTService;
import com.itau.marketplace.services.PasswordService;

@RestController
public class UsuarioController {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired 
	PasswordService passwordService;
	
	@Autowired
	JWTService jwtService;
	
	@RequestMapping(method=RequestMethod.POST,path="/usuario")
	public Usuario inserirUsuario(@RequestBody Usuario usuario) {
		String password  = passwordService.gerarHash(usuario.getPassword());
		usuario.setPassword(password);
		usuario.setId(UUID.randomUUID());
		return usuarioRepository.save(usuario);
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/login")
	public ResponseEntity<?> fazerLogin(@RequestBody Usuario usuario){
		
		Optional<Usuario> optinalUsuario =  usuarioRepository.findByUsername(usuario.getUsername());
		
		
		if(!optinalUsuario.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		
		
		Usuario usuarioBanco = optinalUsuario.get();
		
		if(passwordService.verificarHash(usuario.getPassword(), usuarioBanco.getPassword())) {
			String token = jwtService.gerarToken(usuarioBanco.getUsername());
			
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", String.format("Bearer %s", token));
			headers.add("Access-Control-Expose-Headers", "Authorization");
			
			return new ResponseEntity<Usuario>(usuarioBanco, headers, HttpStatus.OK);
			
		}
		
		
		return ResponseEntity.badRequest().build();
		
	}
	
	@RequestMapping(path="/verificar")
	public ResponseEntity<?> verificarToken(HttpServletRequest request){
		String token = request.getHeader("Authorization");
		token = token.replace("Bearer ", "");
		
		String username = jwtService.verificarToken(token);
		
		if(username == null) {
			return ResponseEntity.status(403).build();
		}
		
		Optional<Usuario> usuarioBanco = usuarioRepository.findByUsername(username);
		
		if(!usuarioBanco.isPresent()) {
			return ResponseEntity.status(403).build();
		}
		
		return ResponseEntity.ok().build();
		
	}
	

}
