package com.itau.marketplace.models;

public class Messagem {
	
	private String mensagem;
	
	public Messagem(String message) {
		setMensagem(message);
	}

	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
}
